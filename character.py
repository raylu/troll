import pygame

from tileset import Tileset

class Character(object):
	SPEED = 2
	movement = [0, 0]

	def __init__(self, start_pos, collisions):
		self.pos = list(start_pos)
		self.render_x = self.pos[0] * Tileset.TILESIZE
		self.render_y = self.pos[1] * Tileset.TILESIZE
		self.collisions = collisions

	def frame(self, keys):
		# are we done moving?
		if self.render_x % Tileset.TILESIZE == 0:
			self.movement[0] = 0
		if self.render_y % Tileset.TILESIZE == 0:
			self.movement[1] = 0

		# can we start moving?
		direction = None
		movement_x = movement_y = None
		if self.movement[1] == 0:
			if (keys[pygame.K_DOWN] or keys[pygame.K_j]):
				movement_y = 1
				direction = Tileset.DOWN
			elif keys[pygame.K_UP] or keys[pygame.K_k]:
				movement_y = -1
				direction = Tileset.UP
			# vertical collision detection
			# each direction is checked separately because holding
			# down+right on a right wall should still move you down
			if movement_y and (self.pos[0], self.pos[1] + movement_y) in self.collisions:
				movement_y = 0
		if self.movement[0] == 0:
			if keys[pygame.K_RIGHT] or keys[pygame.K_l]:
				movement_x = 1
				if self.movement[1] == 0 and movement_y is None: # prefer up/down when moving diagonally
					direction = Tileset.RIGHT
			elif keys[pygame.K_LEFT] or keys[pygame.K_h]:
				movement_x = -1
				if self.movement[1] == 0 and movement_y is None:
					direction = Tileset.LEFT
			# horizontal collision detection
			if movement_x and (self.pos[0] + movement_x, self.pos[1]) in self.collisions:
				movement_x = 0
		if movement_x:
			self.movement[0] = movement_x
			self.pos[0] += movement_x
		if movement_y:
			self.movement[1] = movement_y
			self.pos[1] += movement_y

		# move!
		self.render_x += self.movement[0] * self.SPEED
		self.render_y += self.movement[1] * self.SPEED
		return direction
